# `zonemaster-react-ui`

> **Web interface for [Zonemaster](https://github.com/zonemaster/zonemaster-backend)**

> [<img src="./screenshot.png" alt="screenshot" width="50%" />](https://zonemaster.labs.nic.cz/)

> [zonemaster.labs.nic.cz](https://zonemaster.labs.nic.cz/)

## Technology stack

- React
- [zonemaster-js](https://gitlab.labs.nic.cz/websites/zonemaster-js)
- styled-components

## Running

### Installing Dependencies

```
$ npm install
```

### Configuration

Set backend URL, and optionally Piwik & Sentry URL/ID in `src/config.json`:

```
  …
  "backendUrl": "https://zonemaster.labs.nic.cz/backend/",
  "piwik": {
    "url": "piwik.nic.cz",
    "id": 52
  },
  "sentryDSN": "https://f5077ffc351f4e57adc82d8f587b7f93@sentry.labs.nic.cz/26",
  …
```

### Running a development server

Start a devserver with hot code push:

```
$ npm start
```

(runs at `localhost:3000` by default)

### Adding a new translation

Translations are stored in `app/translations` as simple JSON files. Pick one, copy it to `<lang code>.json`, translate the strings inside it, and add import in `app/lang.js`:

```
$ cp app/translations/en.json app/translations/fr.json
```

lang.js:

```
import en from './translations/en.json';
import cs from './translations/cs.json';
import fr from './translations/fr.json';

const translations = { cs, en, fr };
```

Non-breakable spaces can be inserted as `\u00A0`.

### Building for production

```
$ npm run build
```

Output is placed at `./public`. Build is auto-deployed via Gitlab Pages (if enabled).
