import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Translate, translate } from "react-i18nify";
import backend from "./backend";
import { ReactComponent as Logo } from "./assets/logo.svg";

const FooterContainer = styled.footer`
  border-top: 0.1rem solid #ddd;
  margin-top: 1rem;
  padding: 1rem calc(1rem + 10vw);
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  @media (max-width: 700px) {
    padding: 1rem;
  }
`;

const Text = styled.p`
  flex: 1;
  margin-bottom: 1em;

  a {
    &,
    &:link,
    &:visited {
      color: #333;
    }
  }

  span {
    white-space: nowrap;
    display: inline-block;

    &:not(:first-child) {
      padding-left: 0.33em;
    }
  }
`;

const LogoLink = styled.a`
  color: black;
  text-decoration: none;
  height: 1.5em;
  margin-left: 2em;

  svg {
    height: 1.5em;
    vertical-align: middle;
  }
`;

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.getVersionInfo();
  }

  async getVersionInfo() {
    const version = await backend.versionInfo();
    this.setState({ version });
  }

  render() {
    return (
      <FooterContainer>
        <Text>
          <Translate value="application.footer.text" />{" "}
          <span>
            |{" "}
            <a href={`mailto:${translate("application.footer.mail")}`}>
              <Translate value="application.footer.mail" />
            </a>
          </span>
          {this.state.version ? (
            <span> | backend: {this.state.version.zonemaster_backend} </span>
          ) : null}
          {this.state.version ? (
            <span> | engine: {this.state.version.zonemaster_engine} </span>
          ) : null}
        </Text>
        {this.props.config.logo ? (
          <LogoLink href={this.props.config.logo.link} className="footer-logo">
            <Logo />
          </LogoLink>
        ) : null}
      </FooterContainer>
    );
  }
}

Footer.propTypes = {
  config: PropTypes.shape({
    logo: PropTypes.shape({
      image: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired,
    }),
  }),
};

export default Footer;
